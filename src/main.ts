require('dotenv').config();
process.env['SOURCE'] = 'Juno';
import 'reflect-metadata';
import './loader';

const ApiBuilder = require('claudia-api-builder');
var api = new ApiBuilder();


import { artist, label } from './domain/services';

api.get('/artist/{artist}', async (_request) => {
  return await artist(_request.pathParams.artist);
}, { success: 200 });

api.get('/label/{label}', async (_request) => {
  const releases = await label(_request.pathParams.label);
  console.log(releases);
  return releases;
}, { success: 200 });


module.exports = api;








