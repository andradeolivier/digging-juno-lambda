import { parseJunoListPage } from '../helpers/parse-juno-list-page';
import { DiggingScrapeRequestTypeEnum } from 'digging-scrape';

const artist = async (query) => {
    return await parseJunoListPage(`https://www.juno.co.uk/search/?solrorder=relevancy&facet[daterange][0]=323388000TO${Math.floor(Date.now() / 1000)}&show_out_of_stock=1&q[artist][0]=${encodeURI(query)}`, DiggingScrapeRequestTypeEnum.pageListing);
};

// https://www.juno.co.uk/search/?solrorder=relevancy&q[label][0]=perlon&facet[daterange][0]=323388000TO1588197600&show_out_of_stock=1

export { artist }



