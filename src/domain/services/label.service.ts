import { parseJunoListPage } from '../helpers/parse-juno-list-page';
import { DiggingScrapeRequestTypeEnum } from 'digging-scrape';

const label = async (query) => {
  const url = `https://www.juno.co.uk/search/?solrorder=relevancy&facet[daterange][0]=323388000TO${Math.floor(Date.now() / 1000)}&show_out_of_stock=1&q[label][0]=${encodeURI(query)}`;
  return await parseJunoListPage(url, DiggingScrapeRequestTypeEnum.pageListing);
};

export { label }

