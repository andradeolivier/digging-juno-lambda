import { parseJunoListPage } from '../helpers/parse-juno-list-page';
import { DiggingScrapeRequestTypeEnum } from 'digging-scrape';

const news = async () => {
    let releases = [];
    releases = [...releases, ...await parseJunoListPage('https://www.juno.co.uk/house/this-week/?facet[subgenre_id[[0[=15||9&facet[subgenre_id[[1[=15||6&facet[subgenre_id[[2[=12||3', DiggingScrapeRequestTypeEnum.pageNews)];
    // releases = [...releases, ...await parseJunoListPage(`http://www.juno.co.uk/minimal-tech-house/this-week/?items_per_page=100&show_out_of_stock=1&media_type=vinyl`, DiggingScrapeRequestTypeEnum.pageNews)];
    // releases = [...releases, ...await parseJunoListPage(`http://www.juno.co.uk/techno/this-week/?items_per_page=100show_out_of_stock=1&media_type=vinyl`, DiggingScrapeRequestTypeEnum.pageNews)];
    return releases;
};

export { news }
