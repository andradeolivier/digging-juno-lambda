import { DiggingScrape, DiggingScrapeResponseDto, DiggingScrapeResquestDto } from 'digging-scrape';
import { ReleaseDto, SourceObject } from 'digging-releases';
import { plainToClass } from 'class-transformer';

const parseJunoListPage = async function parseJunoListPage(url, pageType) {
    const request = plainToClass(DiggingScrapeResquestDto, {
      pageUrl: url,
      requestType: pageType,
      source: SourceObject
    });
    const response: DiggingScrapeResponseDto = await DiggingScrape.scrape(request);
    let releases = [];
    if(response && response.releases){
        releases = response.releases.reduce((_releases: ReleaseDto[], r: any)=>{
            r.genres = r.genre.split(', ');
            delete r.genre;
            const release: ReleaseDto = plainToClass(ReleaseDto, r);
            release.source = SourceObject;
            if (release) {
                _releases.push(release.diggingRelease);
            }
            return _releases;
        }, []);
    }
    return releases;
};

export { parseJunoListPage }

