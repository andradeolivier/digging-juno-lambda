import { load } from 'cheerio';
import * as S from 'string';
import * as _ from 'underscore';

const parseRelease = (html, genre) => {
  const $ = load(html);
  const releases: any[] = [];
  if ($('.dv-item').length > 0) {
    $('.dv-item').each(function () {
      let releaseTitle = null;
      let releaseYear = null;
      let releaseDate = null;
      let releaseCover = null;
      let releaseLabel = null;
      let releaseUrl = null;
      let releaseGenre = genre;
      let releaseArtist = null;

      releaseCover = $(this).find('.lazy_img').attr('src');
      if (releaseCover) {
        if (releaseCover.match('data:image')) {
          releaseCover = null;
        } else {
          releaseCover = releaseCover;
        }
      }

      const artistList = [];

      $(this).find('.pl-info').find('.vi-text').each(function () {
        $(this).find('a').each((_k, v) => {
          if ($(v).attr('href').match('/artists/')) {
            artistList.push(S($(v).text()).capitalize().s.trim());
          }

          if ($(v).attr('href').match('/labels/')) {
            releaseLabel = S($(v).text()).capitalize().s.trim();
          }

          if ($(v).attr('href').match('/products/')) {
            releaseTitle = S($(v).text()).capitalize().s.trim();
            releaseUrl = `http://www.juno.co.uk${$(v).attr('href')}`;
          }
        });
      });


      $(this).find('.pl-info').find('.vi-text').each((k, v) => {
        if (k === 4) {
          releaseGenre = $(v).text().trim().replace('/', ', ');
        }

        if (k === 3) {
          const str = $(v).text().trim();
          // @ts-ignore
          if (str.match(/Cat:/, 'i') && str.match(/Rel:/, 'i')) {
            const ex = str.split('Rel:');
            if (ex.length > 0) {
              const date = ex[1].trim();
              const ex1 = date.split(' ');
              if (ex1.length > 2) {
                if (parseInt(ex1[2]) > 60) {
                  releaseYear = `19${ex1[2]}`;
                } else {
                  releaseYear = `20${ex1[2]}`;
                }

                const month = ex1[1];
                const day = ex1[0];

                const months = [];
                months.push('Jan');
                months.push('Feb');
                months.push('Mar');
                months.push('Apr');
                months.push('May');
                months.push('Jun');
                months.push('Jul');
                months.push('Aug');
                months.push('Sep');
                months.push('Oct');
                months.push('Nov');
                months.push('Dec');

                let m: any = _.indexOf(months, month);

                if (m > 0) {
                  if (m < 10) {
                    m = `0${m}`;
                  }
                  releaseDate = `${releaseYear}-${m}-${day}`;
                }
              }
            }
          }
        }
      });

      if (artistList.length < 1) {
        releaseArtist = null;
      } else if (artistList.length < 2) {
        releaseArtist = artistList[0];
      } else {
        releaseArtist = artistList.join(', ');
      }

      const tracks = [];

      $(this).find('.vi-tracklist').find('li').each(function () {
        const t: any = {};
        $(this).find('.vi-text').each(function () {
          let str = $(this).text().trim();
          if (str) {
            const reg = new RegExp(/[(][0-9][:][0-9]{2}[)]/i);
            let matches = str.match(reg);
            if (matches && matches[0]) {
              t.duration = matches[0];
              t.duration = S(t.duration).replaceAll('(', '').s;
              t.duration = S(t.duration).replaceAll(')', '').s;
              const expl = str.split(' ');
              const str2 = [];
              expl.forEach((s) => {
                if (!s.trim().match(reg)) {
                  str2.push(s);
                }
              });
              str = str2.join(' ');
            }

            if (str.match(reg)) {

            }

            t.title = str;

            const explode = t.title.split(' - "');

            matches = t.title.match(new RegExp(/[ ]["].+["]/));

            if (explode.length > 1) {
              t.artist = explode[0];
              t.title = explode[1];
              t.title = S(t.title).replaceAll('"', '').s;
            } else if (matches) {
              t.title = S(matches[0]).replaceAll('"', '').s;
              t.title = S(t.title).replaceAll(' ', '').s;
              t.artist = matches.input.substr(0, matches.index);
            } else {
              t.artist = releaseArtist;
            }
          }
        });
        $(this).find('.vi-icon a.jrplayer').each((_k, v) => {
          t.sample = $(v).attr('href');
        });
        tracks.push(t);
      });

      if (tracks.length > 0) {
        releases.push({
          kind: 'release',
          artist: releaseArtist,
          tracks,
          label: releaseLabel,
          cover: releaseCover,
          source: 'juno',
          url: releaseUrl,
          title: releaseTitle,
          playedBy: releaseArtist,
          year: releaseYear,
          releaseDate,
          genre: releaseGenre,
        });
      }
    });
  }
  return releases;
};

export { parseRelease }
