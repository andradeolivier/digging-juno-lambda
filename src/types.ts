const TYPES = {
    All: Symbol.for("All"),
    Artist: Symbol.for("Artist"),
    Label: Symbol.for("Label"),
    News: Symbol.for("News"),
};

export { TYPES };
