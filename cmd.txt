With Digging-policy:
claudia create --region us-east-1 --api-module index --role digging-full-access --memory 3008 --timeout 20

With new policy:
claudia create --region us-east-1 --api-module build/src/main --policies policy --memory 3008 --timeout 20

Update:
claudia update

Destroy:
claudio destroy ( don't forget to remove the created role if alrealy exists




