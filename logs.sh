#!/bin/bash
aws lambda invoke --function-name digging-juno --payload '{"key": "value"}' out
sed -i'' -e 's/"//g' out
sleep 15
aws logs get-log-events --log-group-name /aws/lambda/digging-juno --log-stream-name $(cat out) --limit 5
